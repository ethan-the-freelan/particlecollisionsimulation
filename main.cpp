/*
 * @project ParticleCollisionSimulation
 * @file	main.cpp
 * @author	Group 9 - Computer Graphics - K54CA
 *
 * @description
 * Simulate the moves of a flow of particles,
 * which can collide with a ground plane and variant number of spheres in 3D space.
 * The program contains a mouse & keyboard event handling function
 * to simulate more scenarios of the collisions.
 */

#ifdef _WIN32
	#include <windows.h>
	#include <sys/timeb.h>
#else
	#include <limits.h>
	#include <unistd.h>
	#include <sys/types.h>
	#include <sys/times.h>
#endif
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <GL/glut.h>

#include "CSphere.h"
#include "CParticle.h"

// Initialize particles array as NULL
CParticle* particles = NULL;

// Initialize position of collide spheres
CSphere spheres[SPHERE_NUM] = {
	CSphere(-0.1, 0.6, 0.0, 0.4),
	CSphere(-0.7, 1.4, 0.0, 0.25),
	CSphere(0.1, 1.5, 0.1, 0.3)
};

// Global declarations
	// Initialize number of particles
int particleNum = 10000,
	
	// Clock ticker used to set time step
	CLOCK_TICKER = 1000,

	// Initialize particle flow mode
	sprayType = PS_WATERFALL,

	// Initialize particle drawing mode
	pointMode = PS_POINT_MODE,

	// Initialize particle size (in point mode)
	pointSize = 4,

	// Initialize number of sphere
	sphereNum = 1,

	// Initialize screen fog mode
	isFogOn = 1,

	// Saved camera position
	oldX, oldY;

float frameTime = 0,
	// Initialize displayed particles in flow
	particleFlow = 500,

	// Initialize speed to slowdown animation when system is weak
	slowDownSpeed = 1,

	// Initialize camera's spin position
	spinX = 0, spinY = 0,

	// Initialize camera's zoom position
	zoom = 0;

/*
 * Returns the number of seconds that have elapsed
 * since the previous call to the function.
 */
float timeDelta(void) {
    static long begin = 0;
    static long finish, difference;

#ifdef _WIN32
	static struct timeb tb;
	ftime(&tb);
	finish = tb.time*1000+tb.millitm;
#else
	static struct tms tb;
	finish = times(&tb);
#endif

	difference = finish - begin;
	begin = finish;

	return (float)difference/(float)CLOCK_TICKER;
}


/*
 * Callback function for window reshape event
 */
void reshapeFunc(int width, int height) {
	float black[] = {0, 0, 0, 0};

	// Set viewport
	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60, (float)width/height, 1, 1000);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(0, 1, 3, 0, 1, 0, 0, 1, 0);

	// Set screen fog
	glFogi(GL_FOG_MODE, GL_LINEAR);
	glFogfv(GL_FOG_COLOR, black);
	glFogf(GL_FOG_START, 2.5);
	glFogf(GL_FOG_END, 4);
	glEnable(GL_FOG);

	// Set display stuffs
	glPointSize(pointSize);
	glEnable(GL_POINT_SMOOTH);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LIGHT0);

	// Set time step
	timeDelta();
}

/*
 * Callback function for display event
 */
void displayFunc(void) {
	int i;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Save current matrix state
	glPushMatrix();
	glRotatef(spinY, 1, 0, 0);
	glRotatef(spinX, 0, 1, 0);
	glTranslatef(0, 0, -zoom);

	// Save current matrix state to draw sphere
	glPushMatrix();
	glEnable(GL_LIGHTING);
	if (sphereNum)
		for (i = 0; i < sphereNum; i++)
			spheres[i].draw();
	glDisable(GL_LIGHTING);

	// Draw ground plane
	glBegin(GL_QUADS);
		glColor3ub(0, 128, 255);
		glVertex3f(-2, 0, -2);
		glVertex3f(-2, 0, 2);
		glVertex3f(2, 0, 2);
		glVertex3f(2, 0, -2);
	glEnd();

	// Draw particle flow
	for (i = 0; i < particleNum; i++)
		particles[i].draw(pointMode);

	glPopMatrix();
	glutSwapBuffers();
}

/*
 * Callback function for animation idle event
 */
void idleFunc(void) {
	static int i, j;
	static int living = 0;			// index to end of live particles
	static float delta;

	delta = timeDelta();
	frameTime += delta;

	// slow the simulation if we can't keep the frame rate up around 10 fps
	if (delta > 0.1)
		slowDownSpeed = 1.0/(100*delta);
	else if (delta < 0.1)
		slowDownSpeed = 1;

	delta *= slowDownSpeed;

	// resurrect a few particles
	for (i = 0; i < particleFlow*delta; i++) {
		particles[living].resurrect(sprayType, delta);
		living++;
		if (living >= particleNum)
			living = 0;
	}

	for (i = 0; i < particleNum; i++) {
		particles[i].timeStep(delta);

		// if collision with sphere
		if (sphereNum)
			for (j = 0; j < sphereNum; j++)
				particles[i].collideSphere(spheres[j]);

		// if collision with ground
		if (particles[i].position.y <= 0)
			particles[i].bounce(delta);

		// if dead particle
		if (particles[i].position.y < 0.1 && particles[i].velocity.y == 0)
			particles[i].alive = false;
	}

	glutPostRedisplay();
}

/*
 * Callback function for visibility event
 */
void visibilityFunc(int state) {
	if (state == GLUT_VISIBLE)
		glutIdleFunc(idleFunc);
	else
		glutIdleFunc(NULL);
}

/*
 * Callback function for window halt event
 */
void haltFunc(int code) {
	free(particles);
	exit(code);
}

/*
 * Callback function for keyboard event
 */
void keyboardFunc(unsigned char key, int x, int y) {
	static int fullscreen = 0;
	static int old_x = 50;
	static int old_y = 50;
	static int old_width = 512;
	static int old_height = 512;
	static int sphere = 0;

	switch (key) {
	case 27:					// escape
		haltFunc(0);
		break;

	case 'w':
		zoom += 0.1;
		break;

	case 's':
		zoom -= 0.1;
		break;

	case 'f':
		if (isFogOn)
			glDisable(GL_FOG);
		else
			glEnable(GL_FOG);
		isFogOn = !isFogOn;
		break;

	case 't':
		if (sprayType == PS_WATERFALL)
			sprayType = PS_FOUNTAIN;
		else if (sprayType == PS_FOUNTAIN)
			sprayType = PS_WATERFALL;
		break;

	case 'a':
		sphereNum++;
		if (sphereNum > SPHERE_NUM)
			sphereNum = 1;
		break;

	case 'l':
		if (pointMode == PS_POINT_MODE)
			pointMode = PS_LINE_MODE;
		else
			pointMode = PS_POINT_MODE;
		break;

	case 'P':
		pointSize++;
		glPointSize(pointSize);
		break;

	case 'p':
		pointSize--;
		if (pointSize < 1)
			pointSize = 1;
		glPointSize(pointSize);
		break;

	case '+':
		particleFlow += 100;
		if (particleFlow > particleNum)
			particleFlow = particleNum;
		break;

	case '-':
		particleFlow -= 100;
		if (particleFlow < 0)
			particleFlow = 0;
		break;

	case '*':
		CLOCK_TICKER += 100;
		if (CLOCK_TICKER > 5000)
			CLOCK_TICKER = 100;
		break;

	case '/':
		CLOCK_TICKER -= 100;
		if (CLOCK_TICKER < 100)
			CLOCK_TICKER = 5000;
		break;

	case '~':
		fullscreen = !fullscreen;
		if (fullscreen) {
			old_x = glutGet(GLUT_WINDOW_X);
			old_y = glutGet(GLUT_WINDOW_Y);
			old_width = glutGet(GLUT_WINDOW_WIDTH);
			old_height = glutGet(GLUT_WINDOW_HEIGHT);
			glutFullScreen();
		} else {
			glutReshapeWindow(old_width, old_height);
			glutPositionWindow(old_x, old_y);
		}
		break;

	case '!':
		sphere++;
		if (sphere >= SPHERE_NUM)
			sphere = 0;
		break;

	case '4':
		spheres[sphere].move(-DEVIATION, 0, 0);
		break;

	case '6':
		spheres[sphere].move(DEVIATION, 0, 0);
		break;

	case '3':
		spheres[sphere].move(0, -DEVIATION, 0);
		break;

	case '9':
		spheres[sphere].move(0, DEVIATION, 0);
		break;

	case '8':
		spheres[sphere].move(0, 0, -DEVIATION);
		break;

	case '2':
		spheres[sphere].move(0, 0, DEVIATION);
		break;

	case '7':
		spheres[sphere].radius += DEVIATION;
		break;

	case '1':
		spheres[sphere].radius -= DEVIATION;
		break;

	default:
		break;
	}
}

/*
 * Callback function for menu event
 */
void menuFunc(int item) {
	keyboardFunc((unsigned char)item, 0, 0);
}

/*
 * Callback function for menu state event
 */
void menuStateFunc(int state) {
    // hook up a fake time delta to avoid jumping when menu comes up
	if (state == GLUT_MENU_NOT_IN_USE)
		timeDelta();
}

/*
 * Callback function for mouse event
 */
void mouseFunc(int button, int state, int x, int y) {
	oldX = x;
	oldY = y;
	glutPostRedisplay();
}

/*
 * Callback function for animation motion event
 */
void motionFunc(int x, int y) {
	spinX = x - oldX;
	spinY = y - oldY;
	glutPostRedisplay();
}

int main(int argc, char** argv) {
	glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE);
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(800, 600);
	glutInit(&argc, argv);
	glutCreateWindow("Particles Collision Simulation");
	glutDisplayFunc(displayFunc);
	glutReshapeFunc(reshapeFunc);
	glutMotionFunc(motionFunc);
	glutMouseFunc(mouseFunc);
	glutKeyboardFunc(keyboardFunc);

	glutMenuStateFunc(menuStateFunc);
	glutCreateMenu(menuFunc);
	glutAddMenuEntry("[f] Fog on/off",					'f');
	glutAddMenuEntry("[t] Spray type",					't');
	glutAddMenuEntry("[s] Collision spheres",			's');
	glutAddMenuEntry("[-] Less flow",					'-');
	glutAddMenuEntry("[+] More flow",					'+');
	glutAddMenuEntry("[p] Smaller points",				'p');
	glutAddMenuEntry("[P] Larger points",				'P');
	glutAddMenuEntry("[l] Toggle points/lines",			'l');
	glutAddMenuEntry("[~] Toggle full screen on/off",	'~');
	glutAddMenuEntry("[Keypad] Move the spheres",		0);
	glutAddMenuEntry("[!] Change active sphere",		0);
	glutAddMenuEntry("[Esc] Quit",						27);
	glutAttachMenu(GLUT_RIGHT_BUTTON);

	if (argc > 1) {
		if (strcmp(argv[1], "-h") == 0)
			exit(0);
		sscanf(argv[1], "%d", &particleNum);
		if (argc > 2)
			sscanf(argv[2], "%f", &particleFlow);
		if (argc > 3)
			sscanf(argv[3], "%f", &slowDownSpeed);
	}

	particles = new CParticle[particleNum];

	glutVisibilityFunc(visibilityFunc);
	glutMainLoop();
	return 0;
}
