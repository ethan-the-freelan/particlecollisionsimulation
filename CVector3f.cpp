/*
 * vector3f.cpp
 *
 *  Created on: Apr 22, 2012
 *      Author: Ethan
 */

#include "CVector3f.h"

CVector3f::CVector3f() {
	x = y = z = 0;
}

CVector3f::CVector3f(float _x, float _y, float _z) {
	x = _x;
	y = _y;
	z = _z;
}

CVector3f& CVector3f::operator =(const CVector3f& v) {
	x = v.x;
	y = v.y;
	z = v.z;
	return (*this);
}

CVector3f& CVector3f::operator +=(const CVector3f& v) {
	x += v.x;
	y += v.y;
	z += v.z;
	return (*this);
}

CVector3f& CVector3f::operator -=(const CVector3f& v) {
	x -= v.x;
	y -= v.y;
	z -= v.z;
	return (*this);
}

CVector3f& CVector3f::operator *=(float f) {
	x *= f;
	y *= f;
	z *= f;
	return (*this);
}

CVector3f& CVector3f::operator /=(float f) {
	x /= f;
	y /= f;
	z /= f;
	return (*this);
}

const CVector3f CVector3f::operator +(const CVector3f& v) const {
	CVector3f ret = (*this);
	ret += v;
	return ret;
}

const CVector3f CVector3f::operator -(const CVector3f& v) const {
	CVector3f ret = (*this);
	ret -= v;
	return ret;
}

const CVector3f CVector3f::operator *(float f) const {
	CVector3f ret = (*this);
	ret *= f;
	return ret;
}

const CVector3f CVector3f::operator /(float f) const {
	CVector3f ret = (*this);
	ret /= f;
	return ret;
}

bool CVector3f::operator ==(const CVector3f& v) const {
	return (x == v.x && y == v.y && z == v.z);
}

bool CVector3f::operator !=(const CVector3f& v) const {
	return ((*this) != v);
}

void CVector3f::setVector(float _x, float _y, float _z) {
	x = _x;
	y = _y;
	z = _z;
}

float CVector3f::module() {
	return sqrt(x*x + y*y + z*z);
}

float* CVector3f::getVector() {
	float* ret = new float[3];
	ret[0] = x;
	ret[1] = y;
	ret[2] = z;
	return ret;
}
