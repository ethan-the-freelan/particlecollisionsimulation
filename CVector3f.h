/*
 * vector3f.h
 *
 *  Created on: Apr 22, 2012
 *      Author: Ethan
 */

#ifndef VECTOR3F_H_
#define VECTOR3F_H_

#include <math.h>
#include <stdlib.h>

class CVector3f {
public:
	float x, y, z;
	
	// Constructors
	CVector3f();
	CVector3f(float, float, float);

	// Overriding operators
	CVector3f& operator =(const CVector3f&);
	CVector3f& operator +=(const CVector3f&);
	CVector3f& operator -=(const CVector3f&);
	CVector3f& operator *=(float);
	CVector3f& operator /=(float);
	const CVector3f operator +(const CVector3f&) const;
	const CVector3f operator -(const CVector3f&) const;
	const CVector3f operator *(float) const;
	const CVector3f operator /(float) const;
	bool operator ==(const CVector3f&) const;
	bool operator !=(const CVector3f&) const;

	// Change vector's elements
	void setVector(float, float, float);

	// Calculate vector's module
	float module();

	// Get array of vector's elements
	float* getVector();
};

// Emulate drand48 in Linux GCC for cross-platform convenience
#ifdef _WIN32
#define drand48() (float)rand()/RAND_MAX
#endif

#endif /* VECTOR3F_H_ */
