/*
 * CSphere.h
 *
 *  Created on: Apr 22, 2012
 *      Author: Ethan
 */

#ifndef CSPHERE_H_
#define CSPHERE_H_

#include "CVector3f.h"

class CSphere {
public:
	// Center point coordinate
	CVector3f center;

	// Sphere radius
	float radius;

	// Constructor
	CSphere(float, float, float, float);

	// Draw sphere in current space state
	void draw(void);

	// Move sphere to a point
	void move(float, float, float);
};

#endif /* CSPHERE_H_ */
