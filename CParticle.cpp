/*
 * CParticle.cpp
 *
 *  Created on: Apr 22, 2012
 *      Author: Ethan
 */

#include <GL/glut.h>
#include "CVector3f.h"
#include "CParticle.h"

CParticle::CParticle() {
	return;
}

CParticle::~CParticle() {
	return;
}

/*
 * Resurrect the particle with specific type and time step
 * Reset all elements at start state
 * @param type: waterfall or fountain
 * @param delta: time step of this resurrection
 */
void CParticle::resurrect(int type, float delta) {
	alive = true;
	if (type == PS_WATERFALL) {
		position.setVector(0.0, 2.0, 0.0);
		previous = position;
		velocity.setVector(-2*(drand48()-0.0), 0.0, 0.5*(drand48()-0.0));
		dampening = 0.45*drand48();
	} else if (type == PS_FOUNTAIN) {
		position.setVector(-0.1, 0.9, 0.0);
		previous = position;
		velocity.setVector(2*(drand48()-0.5), 5.0, 2*(drand48()-0.5));
		dampening = 0.35*drand48();
	}

	this->timeStep(2*delta*drand48());
}

/*
 * Calulate particle's elements when it collide with ground plane
 * @param delta: time step
 */
void CParticle::bounce(float delta) {
	if (!alive)
		return;

	// Because the particle collide with the ground plane
	// we only need to s for a single dimension.
	float *pos = position.getVector(),
			*prev = previous.getVector(),
			*vel = velocity.getVector();

	float s = prev[1]/vel[1];

	pos[0] = prev[0] + vel[0]*s + vel[0]*(delta-s)*dampening;
	pos[1] = -vel[1]*(delta-s)*dampening; // reflect
	pos[2] = prev[2] + vel[2]*s + vel[2]*(delta-s)*dampening;

	// Damp the reflected velocity
	// because when the particle hit something, it lost some energy
	vel[0] *=  dampening;
	vel[1] *= -dampening;		// reflect
	vel[2] *=  dampening;

	position.setVector(pos[0], pos[1], pos[2]);
	velocity.setVector(vel[0], vel[1], vel[2]);
}

/*
 * Calulate particle's elements when it collide with a sphere
 * @param s: the sphere this particle collide with
 */
void CParticle::collideSphere(CSphere s) {
	CVector3f v = this->position - s.center;
	float distance;

	if (!this->alive)
		return;

	// Calulate distance between particle and sphere's center
	distance = v.module();
	if (distance < s.radius) {
		position = s.center + (v/distance) * s.radius;
		previous = position;
		velocity = v/distance;
	}
}

/*
 * Set particle's elements at the next time step
 * @param delta: time step
 */
void CParticle::timeStep(float delta) {
	if (!this->alive)
		return;

	CVector3f vel(0.0, PS_GRAVITY*delta, 0.0);
	velocity += vel;

	previous = position;
	position += velocity * delta;
}

/*
 * Draw the particle with specific mode
 * @param mode: point mode or line mode
 */
void CParticle::draw(int mode) {
	if (!alive)
		return;

	if (mode == PS_POINT_MODE) {
		glBegin(GL_POINTS);
			float c = position.getVector()[1]/2.1*255;
			glColor3ub(c, 128+c*0.5, 255);
			glVertex3fv(position.getVector());
		glEnd();
	} else {
		glBegin(GL_LINES);
			float c = previous.getVector()[1]/2.1*255;
			glColor4ub(c, 128+c*0.5, 255, 32);
			glVertex3fv(previous.getVector());
			glColor4ub(c, 128+c*0.5, 255, 196);
			glVertex3fv(position.getVector());
		glEnd();
	}
}
