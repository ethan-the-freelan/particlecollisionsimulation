/*
 * CSphere.cpp
 *
 *  Created on: Apr 22, 2012
 *      Author: Ethan
 */

#include <GL/glut.h>
#include "CSphere.h"

/*
 * Constructor
 */
CSphere::CSphere(float x, float y, float z, float _radius) {
	center.setVector(x, y, z);
	radius = _radius;
}

void CSphere::draw() {
	float *_center = center.getVector();

	// Save current matrix state
	glPushMatrix();

	// Translate matrix to sphere's center point coordinate
	glTranslatef(_center[0], _center[1], _center[2]);

	// Set sphere surface color
	glColor3ub(0, 255, 128);

	// Draw a solid sphere
	glutSolidSphere(radius, 16, 16);

	// Restore matrix state
	glPopMatrix();
}

void CSphere::move(float x, float y, float z) {
	// Move the center point coordinate
	center.setVector(center.x + x, center.y + y, center.z + z);
}
