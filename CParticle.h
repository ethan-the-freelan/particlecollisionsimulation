/*
 * CParticle.h
 *
 *  Created on: Apr 22, 2012
 *      Author: Ethan
 */

#ifndef CPARTICLE_H_
#define CPARTICLE_H_

#include "CVector3f.h"
#include "CSphere.h"

// Constant gravity acceleration
#define PS_GRAVITY		-9.8

// Waterfall mode of particle flow
#define PS_WATERFALL	0

// Fountain mode of particle flow
#define PS_FOUNTAIN		1

// Point mode of particle drawing
#define PS_POINT_MODE	2

// Line mode of particle drawing
#define PS_LINE_MODE	3

// Number of spheres
#define SPHERE_NUM		3

// Epsilon and deviation to compare 2 float numbers
#define EPSILON			0.01
#define DEVIATION		0.05

class CParticle {
public:
	// Particle's current position
	CVector3f position;

	// Particle's previous position (at last time step)
	CVector3f previous;

	// Particle's velocity (by dimensions in space)
	CVector3f velocity;

	// Dampening (energy lost) of particle when collide with other rigids
	float dampening;

	// Alive state of particle
	bool alive;

	// Constructor
	CParticle();

	// Destructor
	~CParticle();

	// Resurrect a particle
	void resurrect(int, float);

	// Calulate particle's elements when it collide with ground plane
	void bounce(float);

	// Calulate particle's elements when it collide with a sphere
	void collideSphere(CSphere);

	// Set particle's elements at the next time step
	void timeStep(float);

	// Draw the particle
	void draw(int);
};

#endif /* CPARTICLE_H_ */
